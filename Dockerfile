FROM sysrun/rpi-rtl-sdr-base:0.4

MAINTAINER Frederik Granna

WORKDIR /tmp

ENV commit_id 5a03d3505639d2594f488ddc5391be1235c0130e

RUN git clone https://github.com/dgiardini/rtl-ais.git && \
    cd rtl-ais && \
    git reset --hard $commit_id && \
    make 

WORKDIR /tmp/rtl-ais

ENTRYPOINT ["./rtl_ais"]
